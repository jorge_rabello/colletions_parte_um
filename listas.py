idades = [39, 30, 27, 18]

print(f"A lista de idades contém {len(idades)} itens.")
print(f"O primeiro item da lista de idades é {idades[0]}")

for idade in idades:
    print(idade)

idades.append(15)

for idade in idades:
    print(idade)

print(type(idades))

idades.remove(30)

for idade in idades:
    print(idade)

idades.clear()

# print(len(idade)) erro cause lista is empty

idades = [39, 30, 27, 18]
print(28 in idades)
print(30 in idades)

if 30 in idades:
    idades.remove(30)

for idade in idades:
    print(idade)

idades.insert(2, 32)

for idade in idades:
    print(idade)

idades.extend([27, 19])

for idade in idades:
    print(idade)


# for idade in idades:
#     print(idade + 1)

print()
print("IDADES")
print(idades)

print()
print("IDADES NO ANO QUE VEM")

idades_no_ano_que_vem = []

for idade in idades:
    idades_no_ano_que_vem.append(idade + 1)

print(idades_no_ano_que_vem)

# list comprehension para aplicar filtros e transformações

idades_proximo_ano = [(idade + 1) for idade in idades]

print()
print("IDADES PRÓXIMO ANO")
print(idades_proximo_ano)

# apenas idades maiores que 21
maiores_de_vinte_um = [(idade) for idade in idades if idade > 21]

print()
print("MAIORES DE 21")
print(maiores_de_vinte_um)


def proximo_ano(idade):
    return idade + 1


maiores_de_vinteum_proximo_ano = [proximo_ano(idade) for idade in idades if idade > 21]
print()
print("MAIORES DE 21 PRÓXIMO ANO")
print(maiores_de_vinteum_proximo_ano)

print()

# evite passar objetos mutáveis
def faz_processamento_de_visualizacao(lista):
    print(len(lista))
    lista.append(13)


idades = [16, 21, 29, 56, 43]
faz_processamento_de_visualizacao(idades)
print(idades)


def faz_processamento_bugado(lista=[]):
    print(len(lista))
    lista.append(13)


faz_processamento_bugado()
faz_processamento_bugado()
faz_processamento_bugado()
faz_processamento_bugado()
faz_processamento_bugado()


def faz_processamento_certo(lista=None):
    if lista == None:
        lista = list()
    print(len(lista))
    lista.append(13)


faz_processamento_certo()
faz_processamento_certo()
faz_processamento_certo()
faz_processamento_certo()
faz_processamento_certo()
