import array as arr

import numpy as np

myarray = arr.array('d', [1, 3.5])
print(myarray)

narray = np.array([1, 3.5])
print(narray)

print(narray + 3)
