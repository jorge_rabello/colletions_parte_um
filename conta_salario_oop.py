from operator import attrgetter
from functools import total_ordering


@total_ordering
class ContaSalario:
    def __init__(self, codigo):
        self._codigo = codigo
        self._saldo = 0

    def deposita(self, valor):
        self._saldo += valor

    def __str__(self):
        return f"[>>>Codigo {self._codigo} Saldo {self._saldo}"

    def __eq__(self, other):
        if type(other) != ContaSalario:
            return False
        return self._codigo == other._codigo

    def __lt__(self, outro):
        if self._saldo != outro._saldo:
            return self._saldo < outro._saldo
        return self._codigo < outro._codigo


conta_jorge = ContaSalario(1700)
conta_jorge.deposita(500)

conta_maria = ContaSalario(133)
conta_maria.deposita(500)

conta_duda = ContaSalario(3)
conta_duda.deposita(1000)

novas_contas = [conta_jorge, conta_maria, conta_duda]

# order pelo código da conta
for conta in sorted(novas_contas, key=attrgetter("_saldo", "_codigo")):
    print(conta)

print(conta_jorge <= conta_maria)
print(conta_jorge >= conta_maria)
print(conta_jorge == conta_maria)
print(conta_jorge != conta_maria)
