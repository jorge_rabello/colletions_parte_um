from operator import attrgetter

from conta_salario import ContaSalario

idades = [15, 87, 65, 56, 32, 49, 37]

print(idades)

for idade in idades:
    print(idade)

print(len(idades))

print(range(len(idades)))

for i in range(len(idades)):
    print(i)

for i in range(len(idades)):
    print(i, idades[i])

print(enumerate(idades))

print(list(range(len(idades))))
print(list(enumerate(idades)))

for valor in enumerate(idades):
    print(valor)

for indice, idade in enumerate(idades):
    print(f"Indice {indice} Idade {idade}")  # unpacking

usuarios = [
    ('Guilherme', 37, 1981),
    ('Daniela', 31, 1987),
    ('Paulo', 39, 1979)
]

for nome, idade, nascimento in usuarios:
    print(nome)

for nome, idade, nascimento in usuarios:
    print(f"Nome {nome}, Idade: {idade}")

for nome, _, _ in usuarios:
    print(nome)

for nome, _, nascimento in usuarios:
    print(f"Nome {nome}, Nascimento {nascimento}")

idades_ordenadas = sorted(idades)

print(idades_ordenadas)

idades_reversed = reversed(idades)
print(idades_reversed)  # iterador
print(list(idades_reversed))
print(sorted(idades, reverse=True))
print(list(reversed(sorted(idades))))
print(idades)
idades.sort()
print(idades)

nomes = ["Guilherme", "Daniela", "Paulo"]
print(sorted(nomes))

conta_guilherme = ContaSalario(17)
conta_guilherme.deposita(500)

conta_daniela = ContaSalario(3)
conta_daniela.deposita(1000)

conta_paulo = ContaSalario(133)
conta_paulo.deposita(510)

contas = [conta_guilherme, conta_daniela, conta_paulo]

for conta in contas:
    print(conta)


def extrai_codigo(self):
    return self.codigo


# print(sorted(contas, key=extrai_codigo))

# for conta in sorted(contas, key=extrai_codigo):
#     print(conta)

for conta in sorted(contas, key=attrgetter("_codigo")):
    print(conta)

print(conta_guilherme < conta_daniela)
print(conta_guilherme > conta_daniela)

for conta in sorted(contas):
    print(conta)

for conta in sorted(contas, reverse=True):
    print(conta)
