from conta_corr import ContaCorrente

guilherme = ('Guilherme', 37, 1981)
daniela = ('Daniela', 31, 1987)

print(guilherme)
print(daniela)

usuarios = [guilherme, daniela]

print(usuarios)

usuarios.append(('Paulo', 39, 1979))  # listas são mutáveis

print(usuarios)

# usuarios[0][0] = 'Guilherme Silveira' tuplas são imutáveis

conta_do_gui = ContaCorrente(10)
conta_do_gui.deposita(500)

conta_da_dani = ContaCorrente(20)
conta_da_dani.deposita(1000)

contas = (conta_do_gui, conta_da_dani)

print(contas)

for conta in contas:
    print(conta)
