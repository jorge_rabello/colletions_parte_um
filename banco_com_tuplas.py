from conta_corr import ContaCorrente


def main():
    conta_do_gui = ContaCorrente(15)
    print(conta_do_gui)
    conta_do_gui.deposita(500)
    print(conta_do_gui)

    conta_da_dani = ContaCorrente(47865)
    conta_da_dani.deposita(1000)
    print(conta_da_dani)

    contas = (conta_do_gui, conta_da_dani)
    print(contas)

    for conta in contas:
        print(conta)

    contas = (conta_do_gui, conta_da_dani, conta_do_gui)
    print(contas[0])

    conta_do_gui.deposita(100)

    print(contas[0])

    contas[0].deposita(200)
    print(contas[0])


if __name__ == "__main__":
    main()
