from conta import Conta
from conta_corrente import ContaCorrente
from conta_multiplo_salario import ContaMultiploSalario
from conta_salario import ContaSalario

conta1 = ContaSalario(37)
print(conta1)

conta2 = ContaSalario(37)
print(conta2)

print(conta1 == conta2)

contas = [conta1]
print(conta1 in contas)
print(conta2 in contas)

conta3 = ContaCorrente(37)
print(conta1)
print(conta3)
print(conta1 == conta3)

conta4 = ContaMultiploSalario(37)
print(conta1)
print(conta4)
print(conta1 == conta4)

print(isinstance(ContaCorrente(34), Conta))
print(isinstance(ContaCorrente(34), ContaSalario))
print(isinstance(ContaCorrente(34), ContaCorrente))
